pragma solidity ^0.4.4;

contract EventMgmt {
  // Ownership
  // ---------
  address public owner;

  function owned() {
    owner = msg.sender;
  }

  modifier onlyOwner {
    if (msg.sender != owner) throw;
    _;
  }

  function transferOwnership(address newOwner) onlyOwner {
    owner = newOwner;
  }
  //------------------

  // Domain Definitions
  // ------------------
  struct Participant {
    uint id; // from app database
    string name; // e.g. Rolling Stones
  }

  struct DistributionSetting {
    unit id; // auto generated
    Participant participant;
    uint percentage;
  }

  struct TicketType {
    uint id; // auto generated
    uint totalCount;
    uint availableCount; // decrement on a 'first sale' purchase
  }

  // participants that have signed off on event details and distribution settings
  struct DistributionAgreement {
    uint createdTimestamp;
    uint completedTimestamp;
    uint[] agreedParticipantIds;
  }

  struct Event {
    uint id; // from app database
    string name;
    string purchaseStartTime;
    string purchaseEndTime; // could even have different windows of time per ticket type
    bool isEventEditingLocked; // no changes to event settings after this occurs
  }

  struct Order {
    uint id; // auto generated
    uint consumerId; // probably need multiple consumerIds and consumerNames here
    string consumerName;
    uint ticketId; // probably need multiple ticket ids here
  }

  struct TicketHistoryItem {
    uint consumerId;
    uint amountPaid;
  }

  struct Ticket {
    uint id; // auto generated
    TicketType ticketType;
    uint consumerId;
    uint consumerName;
    uint amountPaid;
    TicketHistoryItem[] ticketHistory;
  }

  // Domain Instances
  // ----------------
  Event event;
  uint ticketIds; // defaults to 0 automatically
  mapping (uint => DistributionSetting) distributionSettings;
  uint numTicketTypes;
  mapping (uint => TicketType) ticketTypes;
  mapping (uint => Participant) participants;
  uint numDistributionSettings;
  DistributionAgreement distributionAgreement;
  mapping (uint => Ticket) tickets;
  uint numOrders;
  mapping (uint => Order) orders;

	function EventMgmt() {
	}

  function supplyEventInfo(uint eventId, string eventName, Participant[] participants, DistributionSetting[] distributionSettings, TicketType[] ticketTypes) {
    event = Event({
      id: eventId,
      name: eventName
    });

    for(uint i=0; i < participants.length; i++)
    {
      addParticipant(participants[i]);
    }

    for(uint j=0; j < distributionSettings.length; j++)
    {
      addDistributionSetting(distributionSettings[j]);
    }

    for(uint k=0; k < ticketTypes.length; k++)
    {
      addTicketType(ticketTypes[k]);
    }
  }

  function addParticipant(Participant participant) {
    // TODO - void the current DistributionAgreement and create another
    participants[participant.id] = participant;
  }

  function addTicket(Ticket ticket) {
    // TODO ticket logic
  }

  function addDistributionSetting(DistributionSetting distributionSetting) {
    // TODO - void the current DistributionAgreement and create another
    distributionSettings.id = numDistributionSettings++;
    distributionSettings[distributionSettings.id] = distributionSettings;
  }

  function addTicketType(TicketType ticketType) {
    ticketType.id = numTicketTypes++;
    ticketTypes[ticketType.id] = ticketType;
  }

  /**
   * When a ticket is purchased, an order is created,
   */
	function orderConfirmed(uint consumerId, string consumerName, uint ticketId, uint amountPaid) returns(bool) {
    uint nextOrderNum = numOrders++;
    orders[nextOrderNum] = Order({id: nextOrderNum, consumerId: consumerId, consumerName: consumerName, ticketId: ticketId, amountPaid: amountPaid});
    // TODO logic for storing ticket history on ticket
    tickets[ticketId].consumerId = consumerId;
    tickets[ticketId].consumerName = consumerName;
    tickets[ticketId].amountPaid = amountPaid;
	}
}
